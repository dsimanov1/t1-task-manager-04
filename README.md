# TASK-MANAGER

## DEVELOPER

**NAME**: Dima Simanov

**E-MAIL**: simanov.dima@gmail.com

**E-MAIL**: dsimanov@t1-consulting.ru

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i5-8250U

**RAM**: 16Gb

**SSD**: 512Gb

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
